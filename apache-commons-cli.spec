Name:           apache-commons-cli
Version:        1.4
Release:        8
Summary:        CLI Library provides an API for Java
License:        ASL 2.0
URL:            https://commons.apache.org/proper/commons-cli/
BuildArch:      noarch
Source0:        https://dlcdn.apache.org/commons/cli/source/commons-cli-%{version}-src.tar.gz
Patch0001:      CLI-253-workaround.patch
BuildRequires:  maven-local mvn(junit:junit) mvn(org.apache.commons:commons-parent:pom:)
BuildRequires:  mvn(org.apache.maven.plugins:maven-antrun-plugin)

%description
The Apache Commons CLI library provides an API for parsing command line
options passed to programs. It's also able to print help messages
detailing the options available for a command line tool.

%package        help
Summary:        API documentation for apache-commons-cli
Provides:       apache-commons-cli-javadoc = %{version}-%{release}
Obsoletes:      apache-commons-cli-javadoc < %{version}-%{release}

%description    help
API documentation for apache-commons-cli.

%prep
%autosetup -p1 -n commons-cli-%{version}-src
%mvn_alias : org.apache.commons:commons-cli
%mvn_file : commons-cli apache-commons-cli

%build
%mvn_build

%install
%mvn_install

%files -f .mfiles
%doc LICENSE.txt NOTICE.txt

%files help  -f .mfiles-javadoc
%doc README.md RELEASE-NOTES.txt

%changelog
* Thu Nov 07 2024 shaojiansong <shaojiansong@kylinos.cn> - 1.4-8
- Update project url and source download url

* Wed Nov 16 2022 yaoxin <yaoxin30@h-partners.com> - 1.4-7
- Modify invalid Source

* Thu Dec  5 2019 likexin<likexin4@huawei.com> - 1.4-6
- Pakcage init
